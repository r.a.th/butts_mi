from numpy import *
import logging
from .isi_collectors import _getBOST, _noISI, _noShortestISI
from .dist_comput    import _ftnorms
from scipy.stats     import gaussian_kde as scikde
from scipy.integrate import dblquad      as sci2int


class ButtsMIkde:
    """
# ButtsMIkde
  The class performs the same computations of _Mutual Information_
  in spikes of neural population with a feature distribution as ButtsMI,
  but it uses _kernel density estimator_ for construct $p(\Delta t, \Delta f)$
  distribution and then integrate it.
  
  See ButtsMI class for the discrete histograms version.
  
## Class Parameters/Properties:
    - sh2allisi   = False    # a switch, if True uses only shortest ISI if False uses all non-overlapped ISI
    - reduceBOS   = True     # a switch, if False uses all spikes not just burst onset
    - burstisimax =   2.0    # time interval between consequent spikes (in seconds) 
                             # when spikes should consider as burst
    - isimax      =  None    # limits max ISI if it is not None
    - ftnormord   =    2     # Order of norm between features
    - jitrep      =    3     # number of iterations for each jitter values
    - jitmin      =    0.005 # minimal jitter magnitude in second
    - jitmax      =    3.0   # maximal jitter magnitude in second
    - jitNbin     =   20     # number of bin in jutter test
    - jiterrblk   = False    # raises an error if integration error is higher than MI value
    - jitnegblk   = False    # raises an error if MI value is negative

## Computations:
    1. distances (of given order) between features _ftnorms
    2. Computes none-overlapped ISI for two sources _noISI
    3. Collects list ([dF,dT=ISI])
    4. Creates 2D KDE $P(\Delta f, \Delta t)$
    4. Creates 1D KDE for marginal dF distribution $P(\Delta f)$ from the same list
    4. Creates 1D KDE for marginal ISI distribution $P(\Delta t)$ from the same list
    5. Computes Mutual Information 
       $I[\Delta f, \Delta T]= \iint_{V=(\Delta t,\Delta f)} P(x,y) \log{\frac{P(x,y)}{P(x)P(y)}} dx dy$
    """
    def __init__(self,
            sh2allisi:bool        = False,
            reduceBOS:bool        =  True,
            burstisimax:float     =    2.,
            isimax:(None,float)   =   10.,
            ftnormord:int         =    2 ,
            jitrep:int            =   10 ,
            jitmin:float          =    0.001,
            jitmax:float          =    3.9810717,
            jitNbin:int           =   19,
            jiterrblk:bool        = False,
            jitnegblk:bool        = False
            ):
        self.sh2allisi   = sh2allisi
        self.reduceBOS   = reduceBOS
        self.burstisimax = burstisimax
        self.isimax      = isimax
        self.ftnormord   = ftnormord
        self.jitrep      = jitrep
        self.jitmin      = jitmin
        self.jitmax      = jitmax
        self.jitNbin     = jitNbin
        self.jiterrblk   = jiterrblk
        self.jitnegblk   = jitnegblk
        self.logger      = logging.getLogger("ButtsMIkde")

    def _collect_dFdT(self, fl, spikes):
        """
        _collect_dFdT computes ISI, i.e. dT(s), and 
            creates a list of differences in distance, 
            i.e. dF, and ISI. Returns 2D array Nx2 with (dF,dT) in each
            row.

        Function attributes:
            - fl     - list/array     - list of features for each source
                                        if array sources should be in the 0 axis
            - spikes - list of arrays - spikes for each neuron.
        """
        isi    = _noShortestISI(spikes)  \
                   if self.sh2allisi else\
                 _noISI(spikes)
        dFdT = []
        for e1,e2,inter in isi:
            dF = _ftnorms( array([fl[e1],fl[e2]]), self.ftnormord)
            if dF.shape[0] == 0:
                self.logger.error(f'Cannot compute distance between{e1} and {e2} distance is zero')
                continue
            if dF.shape[0] >  1:
                self.logger.error(f'Cannot compute distance between{e1} and {e2} distance more than one distance')
                continue
            dF = dF[0]
            if not self.isimax is None:
                inter = inter[where(inter<=self.isimax)]
            dfdt = column_stack([ones(inter.shape[0])*dF,inter])
            dFdT.append(dfdt)
        dFdT = row_stack(dFdT)
        return dFdT

    def _zero_nan_kde(self, dF, dT, _kde, _kde_dF, _kde_dT):
        """
        _zero_nan_kde computes
            $$
            p(\Delta f, \Delta t) \log_2 \left(\frac{p(\Delta f, \Delta t)}{p(\Delta f)~p(\Delta t)}\right)
            $$
            If result is not a finite number, function returns 0
        Function attributes:
        - dF      - float    - $\Delta f$
        - dT      - float    - $\Delta t$
        - _kde    - callable - scipy **2D** kernel density estimator
        - _kde_dF - callable - scipy **1D** kernel density estimator for $\Delta f$ distribution
        - _kde_dT - callable - scipy **1D** kernel density estimator for $\Delta t$ distribution
        
        """
        pdF,   = _kde_dF(dF)
        pdT,   = _kde_dT(dT)
        pdFdT, = _kde(array([dF,dT]).T)
        # self.logger.debug(f"_zero_nan_kde(dF={dF:0.4f}, dT={dT:0.4f})= {pdFdT:0.4f}, {pdF:0.4f}, {pdT:0.4f}")
        d = pdFdT*log2( pdFdT/pdF/pdT )
        if not isfinite(d):
            # self.logger.debug(f"_zero_nan_kde(dF={dF:0.4f}, dT={dT:0.4f}): pdFdT={pdFdT:0.4f}, pdF={pdF:0.4f}, pdT={pdT:0.4f} : d={d}")
            d = 0.
        return d
    
    def _computeMI(self, fl, spikes,
            dFmin:(float,None) = None,
            dFmax:(float,None) = None,
            dTmin:(float,None) = None,
            dTmax:(float,None) = None):
        """
        _computeMI is an aggregator:
            - collects list of $p(\Delta f, \Delta t)$
            - creates 2D probability density function based on $p(\Delta f, \Delta t)$ pairs
            - 1D probability density function for $p(\Delta f)$
            - 1D probability density function for$p(\Delta t)$
            - integrates Mutual Information
        Function attributes:
        - fl     - list/array     - list of features for each source
                                        if array sources should be in the 0 axis
        - spikes - list of arrays - spikes for each neuron.
        - dFmin  - float or None - lower  boundary of integration in $\Delta f$ axis.
                                   If None it is set 0.5 of min $\Delta f$
        - dFmax  - float or None - higher boundary of integration in $\Delta f$ axis 
                                   If None it is set 1.1 of max $\Delta f$
        - dTmin  - float or None - lower  boundary of integration in $\Delta t$ axis 
                                   If None it is set 0.5 of min $\Delta t$
        - dTmax  - float or None - higher boundary of integration in $\Delta t$ axis 
                                  If None it is set 1.1 of max $\Delta t$
        """
        
        dFdT = self._collect_dFdT(fl, spikes)
                    
        if dFmin is None: dFmin = amin(dFdT[:,0])*0.5
        if dFmax is None: dFmax = amax(dFdT[:,0])*1.1
        if dTmin is None: dTmin = amin(dFdT[:,1])*0.5
        if dTmax is None: dTmax = amax(dFdT[:,1])*1.1
        #DB>>
        self.logger.info(f"Got dF,dT list of size = {dFdT.shape}")
        self.logger.info(f"dF_min={dFmin:0.2f}, dF_max={dFmax:0.2f}" )
        self.logger.info(f"dT_min={dTmin:0.2f}, dT_max={dTmax:0.2f}" )
        #<<DB
        _kde    = scikde( dFdT.T )
        _kde_dF = scikde( dFdT[:,0].T )
        _kde_dT = scikde( dFdT[:,1].T )
        
        y,_error_ = sci2int(\
            self._zero_nan_kde,
            dFmin, dFmax,
            dTmin, dTmax,
            args=(_kde, _kde_dF,_kde_dT)
        )
        return y,_error_
 
 
 ### Jitter ###
    def _jitter(self, ft, spikes,
            jitrep:(int  ,None)  = None,
            jitmin:(float,None)  = None,
            jitmax:(float,None)  = None,
            jitNbin:(int ,None)  = None,
            dFmin:(float, None)  = None,
            dFmax:(float, None)  = None,
            dTmin:(float, None)  = None,
            dTmax:(float, None)  = None
            ):
        if jitrep  is None: jitrep  = self.jitrep
        if jitmin  is None: jitmin  = self.jitmin
        if jitmax  is None: jitmax  = self.jitmax
        if jitNbin is None: jitNbin = self.jitNbin
       

        jitrange = logspace(log10(jitmin),log10(jitmax), jitNbin)
        jittest = hstack([
                arange(jitrange.shape[0],dtype=int) for _ in range(jitrep)
            ])
        
        
        jMI = zeros(jittest.shape[0])
        for i,jit in enumerate(jittest):
            jspikes = [
                sort( abs( s+jitrange[jit]*random.randn(s.shape[0]) ) )
                for s in spikes
            ]
            jMI[i],_error_ = self._computeMI( ft, jspikes, dFmin, dFmax, dTmin, dTmax)
            if _error_ > jMI[i]:
                self.logger.warning(f"Integration error is higher that mutual information value: {_error_} > {jMI[i]}")
                if self.jiterrblk:
                    raise RuntimeError(f"Integration error is higher that mutual information value: {_error_} > {jMI[i]}")
            if jMI[i] < 0. :
                self.logger.warning(f"Mutual Information is bellow zero {jMI[i]}")
                if self.jitnegblk:
                    raise RuntimeError(f"Mutual Information is bellow zero {jMI[i]}")
            self.logger.info(f"Computed {i+1}th jitter amplitude of the {jittest.shape[0]}: MI={jMI[i]}, ERROR={_error_}")
        if jitrep > 1:
            jMI = array(jMI).reshape(jitrep,jitNbin).T
            return jitrange,jMI,mean(jMI,axis=1),std(jMI,axis=1)
        else : 
            return jitrange,jMI,None


    def __call__(self, ft, spikes):
        if self.reduceBOS:
            spikes   = [ array([s for s,n,l in o]) for o in _getBOST(spikes,self.burstisimax)] 
            self.logger.info(f"Burst Filtered")
        ret          = self._computeMI(ft,spikes)
        if ret is None: return None
        MI,_error_ = ret
        self.logger.info(f"MI computed {MI}, {_error_}")
        jitrange,jMI, meanjMI, stdjMI = self._jitter(ft,spikes)
        return MI,jitrange,meanjMI,stdjMI       
