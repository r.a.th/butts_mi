from numpy import *
def _ftnorms(ftarray, order:int):
    """
    _ftdist computes feature norm (distance) of given order
    
    Function attributes:
        - ftarray - numpy array of features with  
                    objects in 0-axis
        - order   - int - order of norm
    """
    return array([
        ( sum( (abs(x-y))**order ) )**float(1/order)
        for xid,x in enumerate(ftarray)
        for y in ftarray[xid+1:]
    ])
