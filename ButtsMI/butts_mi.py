from numpy import *
import logging
from .isi_collectors import _getBOST, _noISI, _noShortestISI
from .dist_comput    import _ftnorms

class ButtsMI:
    """
# ButtsMI 
  The class performs classical Butts's analysis of the 
                _Mutual Information_
  in spikes of neural population with a feature distribution.
  In Butts's paper a feature is a position of the neuron in 2D space, 
  but it may be any real-value parameter in $R^n$ space.
  Ref:[Butts DA, Rokhsar DS. 2001. 
  The information content of spontaneous retinal waves. 
  The Journal of Neuroscience 21:961–973.](https://doi.org/10.1523/JNEUROSCI.21-03-00961.2001)
  
  This class is for the discrete histograms.
  See ButtsMIkde class for 2D continues kernel density estimators.
  
## Class Parameters/Properties:
    - sh2allisi   = False    # a switch, if True uses only shortest ISI if False uses all non-overlapped ISI
    - tp2maxisi   = 0        # a switch, if 0 uses fixed isiNbins. Returns an error if  M/(Nt*Nd) > 10
                             #           if 1 decreases max ISI and increases min ISI to get timeprecision under constrains t_inbin_min/t_inbin_max
                             #           if 2 decreases number of bins with fixed maxisi also under constrains t_inbin_min/t_inbin_max
                                              this option may not work if sources are firing in deterministic exact sequence.
    - reduceBOS   = True     # a switch, if False uses all spikes not just burst onset
    - CorrectMI   = False    # a switch, if True does correction of MI which can lead to negative numbers.
    - BlockCorrect= False    # a switch, if True raises RuntimeError if correction makes MI negative. If False returns uncorrected MI
    - jTparallel  = True     # a switch, if True compute jitter in parallel
    - burstisimax =   2.0    # time interval between spikes (in seconds) when spikes should consider as burst
    - d_inbin_min =   20     # min boundary on a min number of distances in any bin for the distance distribution
    - d_inbin_max =   70     # max boundary on a min number of distances in any bin for the distance distribution
    - t_inbin_min =    7     # min boundary on a min number of ISI in any bin for the ISI distribution
    - t_inbin_max =   70     # max boundary on a min number of ISI in any bin for the ISI distribution
    - isimax      =   10.    # limits max ISI
    - timeprec    =    0.004 # 0.01 # in seconds time precision 
    - isiNbins    = 2000     # number of bin in ISI histogram
    - ftnormord   =    2     # Order if norm between features
    - jitrep      =   10     # number of iterations for each jitter values
    - jitmin      =    0.005 # minimal jitter magnitude in second
    - jitmax      =    3.0   # maximal jitter magnitude in second
    - jitNbin     =   20     # number of bin in jutter test

## Computations:
    1. distances (of given order) between features _ftnorms
    2. distribution (normalized histogram) of distances between features $p(\Delta f)$ _fthist
    3. Computes none-overlapped ISI for two sources _noISI
    4. Collects all ISI that are from the sources that are at specific distance $p(\Delta f)$ _collect_dF_ISI
    5. For each bin in distance distribution, computes conditional distribution of ISI, $p(\Delta t|\Delta f)$ : _fixed_PdTdF, _nvar_PdTdF
    6. Computes distribution of ISI $p(\Delta t) = \sum_{\Delta f} p(\Delta f) p(\Delta t|\Delta f)$: _compute_PdT
    7. Computes Mutual Information 
       $I[\Delta f, \Delta T]= \sun_{\Delta f} p(\Delta f) \sum_{\Delta t} p(\Delta t|\Delta f) \log_2 \frac{p(\Delta t|\Delta f)}{p(\Delta t)}$ : _computeMIfromPdTdF
    """
    def __init__(self,
            sh2allisi:bool        = False,
            tp2maxisi:int         = 0,
            reduceBOS:bool        = True,
            CorrectMI:bool        = False,
            BlockCorrect:bool     = False,
            jTparallel:(bool,int) = True,
            burstisimax:float     =    2.,
            d_inbin_min:int       =   20 ,
            d_inbin_max:int       =   70 ,
            t_inbin_min:int       =    7 ,
            t_inbin_max:int       =   70 ,
            isimax:(None,float)   =   10.,
            timeprec:float        =    0.004,
            isiNbins:int          = 2000 ,
            ftnormord:int         =    2 ,
            jitrep:int            =   10 ,
            jitmin:float          =    0.001,
            jitmax:float          =    3.9810717,
            jitNbin:int           =   19
            ):
        self.sh2allisi   = sh2allisi
        self.tp2maxisi   = tp2maxisi
        self.reduceBOS   = reduceBOS
        self.CorrectMI   = CorrectMI
        self.BlockCorrect= BlockCorrect
        self.jTparallel  = jTparallel
        self.burstisimax = burstisimax
        self.d_inbin_min = d_inbin_min
        self.d_inbin_max = d_inbin_max
        self.t_inbin_min = t_inbin_min
        self.t_inbin_max = t_inbin_max
        self.isimax      = isimax
        self.timeprec    = timeprec
        self.isiNbins    = isiNbins
        self.ftnormord   = ftnormord
        self.jitrep      = jitrep
        self.jitmin      = jitmin
        self.jitmax      = jitmax
        self.jitNbin     = jitNbin
        self.logger      = logging.getLogger("ButtsMI")
    
    def _nvar(self, xarray , xmin:float  , xmax:float,
                            inbin_min:int, inbin_max:int,
                            task:str, nbininit:(int,None) = None):
        """
        _nvar adjusts number of bins in histogram until minimal number
              of objects in a bit is more than inbin_min but less
              than inbin_max
        Function attributes:
            - xarray     - 1D numpy array
            - xmin, xmax - defines histogram range (float, float)
            - inbin_min  - minimal boundary, if min number of objects
                           less than that, _nvar decreases number of bins
            - inbin_max  - maximal boundary, if min number of objects
                           more than that, _nvar increases number of bins
            - task       - string - the name of the general task which calls
                           _nvar method.
            - nbininit  - initial guess of the number of bins
        """
        
        nmin,atmcnt,nbins = -1,0,\
            (xarray.shape[0]//10 if nbininit is None else nbininit)
        while (nmin < inbin_min or nmin > inbin_max) and atmcnt < 100000 :
            hist,bins = histogram(xarray, bins=nbins, range=(xmin,xmax) )
            nmin      = amin(hist)
            self.logger.debug(f'{task}:  iner:{atmcnt}, nbins={nbins}, inbin_min={inbin_min},  nmin={nmin}, inbin_max={inbin_max}')
            if   nmin < inbin_min :
                nbins  -= 1
                atmcnt += 1
                if nbins < 3:
                    self.logger.error(f"Cannot fit histogram size for {task}:  nbins={nbins} < 3")
                    return None,None,None
            elif nmin > inbin_max :
                nbins  += 1
                atmcnt += 1
        if atmcnt >= 100000:
            self.logger.error(f"Cannot fit histogram size for {task}:  number of iterations exceeds 100k! Current stage inbin_min={inbin_min},  nmin={nmin}, inbin_max={inbin_max}")
            return None,None,None
        return hist, bins, nbins

    def _fthist(self, ftarray ):
        """
        _fthist computes histogram of "distances" between features
        
        Function attributes:
            - ftarray - numpy array of features with  
                        objects in 0-axis        
        """
        dist      = _ftnorms(ftarray, self.ftnormord)
        dmin,dmax = amin(dist),amax(dist)
        hist, bins, nmin = _nvar(dist,dmin,dmax,self.d_inbin_min,self.d_inbin_max,"_fthist")
        return hist, bins


    def _collect_dF_ISI(self,p_dF, dFbins,isi,ft):
        """
        _collectDISI collects isi in distribution of feature distances

        Function attributes:
            - p_dF   - array of distance histogram
            - dFbins - array of distance bins of p_dF
            - isi    - list of 
            - ft     - list of features of each source
        
        Returns:
           list of ISI for each bin in feature distance distribution
        """
        dF_ISI  = [ [] for h in p_dF ] 
        for e1,e2,inter in isi:
            dist = _ftnorms( array([ft[e1],ft[e2]]), self.ftnormord )
            if dist.shape[0] == 0:
                self.logger.error(f'Cannot compute distance between{e1} and {e2} distance is zero')
                continue
            if dist.shape[0] >  1:
                self.logger.error(f'Cannot compute distance between{e1} and {e2} distance more than one distance')
                continue
            dist = dist[0]
            idx, = where( logical_and(dFbins[:-1]<=dist,dFbins[1:]>dist) )
            if idx.shape[0] == 0:
                if dist == dFbins[-1]: idx = array([-1],dtype=int)
                else:
                    self.logger.error(f'Cannot find bin for distance between {e1} and {e2} with distance between them {dist}:{dFbins}')
                    continue
            if idx.shape[0] >  1:
                self.logger.error(f'More than one bin for distance between {e1} and {e2} with distance between them {dist}:{dFbins}')
                continue
            idx = idx[0]
            dF_ISI[idx] += inter.tolist()
        return dF_ISI

    def _pre_dF_ISI(self, p_dF, dFbins,isi,ft):
        """
        _preDISI computes minimal ISI sequence and found minimal and maximal
           ISIs. If self.isimax is not None: self.isimax limits maximum

        Function attributes:
            - p_dF   - array of distance histogram
            - dFbins - array of distance bins of p_dF
            - isi    - list of 
            - ft     - list of features of each source
        
        Returns:
           list of 
        """
        dF_ISI     = self._collect_dF_ISI(p_dF, dFbins,isi,ft)
        minsize   = None
        tmin,tmax = None, None
        dFnbins   = len(p_dF)
        for idx in range(dFnbins):
            dF_ISI[idx] = array(sorted(dF_ISI[idx]))
            if minsize is None:
                minsize   = dF_ISI[idx][:]
                tmin,tmax = amin(dF_ISI[idx]), amax(dF_ISI[idx])
            else:
                xmin,xmax = amin(dF_ISI[idx]), amax(dF_ISI[idx])
                minsize   = dF_ISI[idx][:] if minsize.shape[0] > dF_ISI[idx].shape[0] else minsize
                tmin      = xmin            if xmin < tmin                              else tmin
                tmax      = xmax            if xmax > tmax                              else tmax
        if self.isimax is not None:
            if tmax > self.isimax: tmax = self.isimax
        return dF_ISI,minsize,tmin,tmax

# def getPtd_minmax(dh,db,isi,epos):
    # p_td_,minsize,tmin,tmax = _prePtd(dh,db,isi,epos)
    # while tmin < tmax :
        # nbins     = int( round( (tmax - tmin) / timeprec ) )
        # if nbins < 3:
            # print("DB #5>> nbins=",nbins)
            # return None
        # hist,bins = histogram(p_td_[0],bins=nbins,range=(tmin,tmax) )
        # intminbin, intmaxbin = hist[0], hist[-1]
        # for isi in p_td_[1:]:
            # hist,bins = histogram(isi, bins=nbins,range=(tmin,tmax) )
            # if intminbin > hist[ 0]: intminbin = hist[ 0]
            # if intmaxbin > hist[-1]: intmaxbin = hist[-1]
        # if intminbin >= t_inbin_min and intmaxbin >= t_inbin_min: break
        # if intminbin < t_inbin_min: tmin += timeprec
        # if intmaxbin < t_inbin_min: tmax -= timeprec
        # #DB>>
        # print("DB #6>>", tmin,tmax,":",intminbin,intmaxbin,nbins)
        # #<<DB
    # if tmin < tmax :
        # nbins = int( round( (tmax - tmin) / timeprec ) )
        # p_td_ = [ histogram(isi, bins=nbins, range=(tmin,tmax) )[0] for isi in p_td_]
        # p_td_ = [ isi/sum(isi) for isi in p_td_]
        # return bins, p_td_        
    # else:
        # return None

    def _fixed_PdTdF(self,  p_dF, dFbins, isi, ft):
        """
        _fixed_Pd  collects ISI in fixed size 2D histogram feature_distance x ISI_histogram
        """
        dF_ISI = self._collect_dF_ISI( p_dF, dFbins,isi,ft)
        
        # p_dTdF_ = [ histogram(array(sorted(isi)), bins=self.isiNbins, range=(0.,self.isimax) ) for isi in dF_ISI]
        # Note >>>
        # This code takes care of ISI which can fell off the histogram with an increase of jitter.
        # It recomputes number of bins in dT histogram, so the number of dT in the first
        # self.isiNbins will be the same.
        effmax = self.isimax+self.jitmax*pi
        effN   = int( ceil(self.isiNbins*effmax/self.isimax) )
        p_dTdF_ = [ histogram(array(sorted(isi)), bins=effN, range=(0.,effmax) ) for isi in dF_ISI]
        # <<<
        dTbins  = copy(p_dTdF_[0][1])
        p_dTdF_ = [ dThist/sum(dThist) for dThist,_ in p_dTdF_]
        return dTbins, p_dTdF_
 
    def _nvar_PdTdF(self, p_dF, dFbins,isi,ft):
        """
        _nvar_Pd collects ISI and find feature-distance bin with fewest 
          number of ISIs. Then it uses _nvar to dins nbins for ISI 
          histogram using self.t_inbin_min,self.t_inbin_max as thresholds.
          Then it computes 2D histogram feature_distance x ISI_histogram

        Function attributes:
            - p_dF   - array of distance histogram
            - dFbins - array of distance bins of dh
            - isi    - list of 
            - ft     - list of features of each source
        
        Returns:
           list of ISI probability distributions (normalized histogram)
           each sorted by bins in feature distance histogram
        """
        dF_ISI,minsize,isimin,isimax = self._pre_dF_ISI(p_dF,dFbins,isi,ft)
        #DB>>
        # print(minsize)
        # print(isimin,isimax)
        # for dfisi in dF_ISI:
            # print('\t',len(dfisi),dfisi)
        #<<DB
 
        hist, bins, nbins = self._nvar(minsize,isimin,isimax,
            self.t_inbin_min,self.t_inbin_max,
            "ISI smallest distribution")
        if nbins is None:
            raise RuntimeError("Cannot fit distribution size")
        if self.CorrectMI:
            NdF = float(p_dF.shape[0])
            M   = sum([len(s) for s in dF_ISI])
            C   = (NdF*nbins-nbins-NdF+1)/M/log(2)
            while C > 0.01:
                nbins  = nbins - 1
                C   = (NdF*nbins-nbins-NdF+1)/M/log(2)
                if nbins < 3:
                    raise RuntimeError("Cannot fit ISI histogram size to meet Correction < 0.01")
                    
        # p_dTdF_ = [ histogram(array(sorted(isi)), bins=nbins, range=(isimin,isimax) ) for isi in dF_ISI]
        # Note >>>
        # This code takes care of ISI which can fell off the histogram with an increase of jitter.
        # It recomputes number of bins in dT histogram, so the number of dT in the first
        # nbins will be the same.
        effmax = isimax+self.jitmax*pi
        effN   = int( ceil(nbins*effmax/isimax) )
        p_dTdF_ = [ histogram(array(sorted(isi)), bins=effN, range=(isimin,effmax) ) for isi in dF_ISI]
        # <<<
        dTbins  = copy(p_dTdF_[0][1])
        p_dTdF_ = [ dThist/sum(dThist) for dThist,_ in p_dTdF_]
        return dTbins, p_dTdF_
    
    def _compute_PdT(self, p_dF, tbinsize:int, p_dT_dF):
        p_dT = zeros(tbinsize)
        for idx in range(tbinsize):
            """
            $$
            p(\Delta t) = \sum_{\Delta f} p(\Delta f)p(\Delta t| \Delta f)
            $$
            """
            for pdf,_pdtdf in zip(p_dF,p_dT_dF):
                p_dT[idx] += pdf*_pdtdf[idx]
        return p_dT

    def _computeMIfromPdTdF(self, p_dF, p_dT, p_dT_dF):
        MI = 0
        for didx, pdf in enumerate(p_dF):
            MI += pdf * nansum( p_dT_dF[didx] * log2(p_dT_dF[didx]/p_dT) )
        return MI
    
    def _computeMI(self, ft, p_dF, dFbins, spikes, thist_prm:(dict,None)=None, CorrectMI:(bool,None)=None, BlockCorrect:(bool,None)=None):
        isi    = _noShortestISI(spikes)\
                   if self.sh2allisi else\
                 _noISI(spikes)
        if thist_prm is None:
            if   self.tp2maxisi == 0:
                M = 0
                for chisi in isi:
                    M += chisi[-1].shape[0]
                if M/p_dF.shape[0]/self.isiNbins < 10:
                    self.logger.error( f"(M={M})/(Nd={p_dF.shape[0]})/(Nt={self.isiNbins}) = {M/p_dF.shape[0]/self.isiNbins} < 10" )
                    return None
                p_dT_dF  = self._fixed_PdTdF(p_dF,dFbins,isi,ft) 
            # elif self.tp2maxisi == 1:
                # p_dT_dF  = self.getPtd_minmax(p_dF,dFbins,isi,ft) 
            elif self.tp2maxisi == 2:
                p_dT_dF  = self._nvar_PdTdF(p_dF,dFbins,isi,ft)
            if p_dT_dF is None : return None
            dTbins,p_dT_dF = p_dT_dF
        else:
            if type(thist_prm) is not dict:
                self.logger.error('Time histogram parameters should be a dictionary')
                raise   TypeError('Time histogram parameters should be a dictionary')
            for name in 'tmin tmax nbins'.split():
                if not name in thist_prm:
                    self.logger.error(f'{name} must be in the thist_prm dictionary')
                    raise  ValueError(f'{name} must be in the thist_prm dictionary')
            p_dF_ISI = self._collect_dF_ISI(p_dF,dFbins,isi,ft)
            p_dT_dF = [ histogram(array(sorted(isi)), bins=thist_prm['nbins'], range=(thist_prm['tmin'],thist_prm['tmax']) )[0] for isi in p_dF_ISI]
            p_dT_dF = [ dThist/sum(dThist) for dThist in p_dT_dF]
            dTbins  = linspace(thist_prm['tmin'],thist_prm['tmax'],thist_prm['nbins']+1)

        p_dT = self._compute_PdT(p_dF, dTbins.shape[0]-1, p_dT_dF)
        #Correction Equ. 2:
        if CorrectMI    is None: CorrectMI    = self.CorrectMI
        if BlockCorrect is None: BlockCorrect = self.BlockCorrect
        if CorrectMI:
            NdF, NdT    = float(p_dF.shape[0]),float(p_dT.shape[0])
            TotalSample = sum([ int(len(s)) for s in isi ])
            Correction = (NdF*NdT-NdF-NdT+1)/2/TotalSample/log(2)
            MI = self._computeMIfromPdTdF(p_dF, p_dT, p_dT_dF)
            if MI > Correction:
                MI -= Correction
            else:
                self.logger.error(f'Correction too big, MI is negative. NdF={NdF}, NdT={NdT}, M={TotalSample}, Correction={Correction}, uncorrect MI={MI}')
                if BlockCorrect:
                    raise TimeoutError(f'Correction too big, MI is negative. NdF={NdF}, NdT={NdT}, M={TotalSample}, Correction={Correction}, uncorrect MI={MI}')            
        else:
            MI = self._computeMIfromPdTdF(p_dF, p_dT, p_dT_dF)
        return isi,p_dT_dF,dTbins,MI
    

### Jitter ###
    def _pjit(self, xjit):
        """
        parallel worker for jitter 
        """
        jit,(ft, p_dF, dFbins, spikes, thist_prm) = xjit
        
        jspikes = [
            sort( abs( s+jit*random.randn(s.shape[0]) ) )
            # abs( s + jit*(2*random.rand(s.shape[0])-1) )
            for s in spikes
        ]
        _,_,_,mi = self._computeMI(ft, p_dF, dFbins,jspikes,thist_prm,False,False)
        return mi


    def _jitter(self, ft, p_dF, dFbins, spikes, thist_prm:dict,
            jitrep:(int  ,None)  = None,
            jitmin:(float,None)  = None,
            jitmax:(float,None)  = None,
            jitNbin:(int ,None)  = None
            ):
        for name in 'tmin tmax nbins'.split():
                if not name in thist_prm:
                    self.logger.error(f'{name} must be in the thist_prm dictionary')
                    raise  ValueError(f'{name} must be in the thist_prm dictionary')
        if jitrep  is None: jitrep  = self.jitrep
        if jitmin  is None: jitmin  = self.jitmin
        if jitmax  is None: jitmax  = self.jitmax
        if jitNbin is None: jitNbin = self.jitNbin
       

        jitrange = logspace(log10(jitmin),log10(jitmax), jitNbin)
        jittest = hstack([
                arange(jitrange.shape[0],dtype=int) for _ in range(jitrep)
            ])
        
        
        if self.jTparallel:
            import multiprocessing as mps
            if type(self.jTparallel) is int:
                with mps.Pool(self.jTparallel) as p:
                    jMI = p.map(self._pjit, zip(jitrange[jittest],[ (ft, p_dF, dFbins, spikes, thist_prm) for i in jittest]))
            else:
                with mps.Pool() as p:
                    jMI = p.map(self._pjit, zip(jitrange[jittest],[ (ft, p_dF, dFbins, spikes, thist_prm) for i in jittest]))
        else:
            jMI = zeros(jittest.shape[0])
            for i,jit in enumerate(jittest):
                jMI[i] = self._pjit([jitrange[jit],(ft, p_dF, dFbins, spikes, thist_prm)])
        if jitrep > 1:
            jMI = array(jMI).reshape(jitrep,jitNbin).T
            return jitrange,jMI,mean(jMI,axis=1),std(jMI,axis=1)
        else : 
            return jitrange,jMI,None


    def __call__(self, ft, spikes):
        dist = _ftnorms(ft, self.ftnormord)
        dmin,dmax = amin(dist),amax(dist)
        dh,db,_   = self._nvar(dist,dmin,dmax,self.d_inbin_min,self.d_inbin_max,"Feature Distances")
        if dh is None: return None
        dh        = dh/sum(dh)
        if self.reduceBOS:
            spikes   = [ array([s for s,n,l in o]) for o in _getBOST(spikes,self.burstisimax)] 
        ret       = self._computeMI(ft,dh,db,spikes)
        if ret is None: return None
        ibi,p_td_,tbins,MI = ret
        jitrange,jMI, meanjMI, stdjMI = self._jitter(ft,dh,db,spikes,{
            'tmin'  : amin(tbins),
            'tmax'  : amax(tbins),
            'nbins' : tbins.shape[0]
        })
        return MI,jitrange,meanjMI,stdjMI
