from numpy import *
import logging

def _getBOST(spikes, burstisimax:float):
    """
    _getBOST find burst on set time

    Function attributes:
        - spikes      - list of ndarray with spikes for each object
        - burstisimax - float number - the maximal ISI to consider two
                        spikes are belong the same spike burst
    
    Returns a list of 2D-arrays with 3 columns:
     time of first spike in burst, time of last spike in burst, and number of  spikes in burst
    """
    BOST = []
    for spk in spikes:
        BOST.append([])
        Sburst, Lburst, Nburst = spk[0],spk[0],1
        for s in spk[1:]:
            if Lburst+burstisimax > s:
                Lburst  = s
                Nburst += 1
            else:
                if Nburst > 1:
                    BOST[-1].append( [Sburst, Lburst, Nburst])
                Sburst, Lburst, Nburst = s,s,1
        BOST[-1] = array(BOST[-1])
    return BOST

def _noISI(spikes):
    """ 
    _noISI finds all none-overlapped inter-spike intervals
    for spikes form different neurons
    
    Function attributes:
        - spikes - list of ndarray with spikes for each object      
    
    Returns a list of with 3 values in the each entry:
     id of first neuron,
     id of second neuron,
     array of shortest ISIs
    
    Note: This algorithm is a crawler, jumps from one spike 
      to another. It does not find all possible ISI, just ISI of
      closet to each other spikes.
    """
    isi = []
    for xi, x in enumerate(spikes):
        for yi,y in enumerate(spikes[xi+1:]):
            isi.append( [xi,xi+yi+1,[]] )
            ix,iy = 0, 0
            if ix+1 >= x.shape[0] or iy+1 >= y.shape[0]: 
                isi[-1][2] = array(isi[-1][2])
                continue
            x2y   = x[0] < y[0]
            while True:
                if ix+1 >= x.shape[0]: break
                if iy+1 >= y.shape[0]: break
                if x2y:
                    while not(x[ix] <= y[iy] and x[ix+1] > y[iy]): 
                        isi[-1][2].append(y[iy]-x[ix])
                        ix += 1
                        if ix+1 == x.shape[0]: break
                    else:
                        isi[-1][2].append(y[iy]-x[ix])
                        ix += 1
                    if ix+1 >= x.shape[0]: break 
                    if y[iy+1] > x[ix+1]:
                        isi[-1][2].append(x[ix]-y[iy])
                        iy += 1
                        if iy+1 == y.shape[0]: break
                    else:
                        x2y = False
                else:
                    while not(y[iy] <= x[ix] and y[iy+1] > x[ix]):
                        isi[-1][2].append(x[ix]-y[iy])
                        iy += 1
                        if iy+1 == y.shape[0]: break
                    else:
                        isi[-1][2].append(x[ix]-y[iy])
                        iy += 1
                    if iy+1 >= y.shape[0]: break
                    if x[ix+1] > y[iy+1]:
                        isi[-1][2].append(y[iy]-x[ix])
                        ix += 1
                        if ix+1 == x.shape[0]: break
                    else:
                        x2y = True                
            isi[-1][2] = array(isi[-1][2])
    return isi
        
def _noShortestISI( spikes):
    """ 
    _noShortestISI finds shortest none-overlapped inter-spike intervals
    for spikes form different neurons
    
    Function attributes:
        - spikes - list of ndarray with spikes for each object      
    
    Returns a list of with 3 values in the each entry:
     id of first neuron,
     id of second neuron,
     array of shortest ISIs
    
    Note: The same as for _noISI, but returns only shortest if two 
      or more ISI exist for a given spike.
    """
    isi = []
    for xi, x in enumerate(spikes):
        for yi,y in enumerate(spikes[xi+1:]):
            isi.append( [xi,xi+yi+1,[]] )
            ix,iy = 0, 0
            if ix+1 >= x.shape[0] or iy+1 >= y.shape[0]: 
                isi[-1][2] = array(isi[-1][2])
                continue
            x2y   = x[0] < y[0]
            while True:
                if ix+1 >= x.shape[0]: break
                if iy+1 >= y.shape[0]: break
                if x2y:
                    while not(x[ix] <= y[iy] and x[ix+1] > y[iy]): 
                        ix += 1
                        if ix+1 == x.shape[0]: break
                    isi[-1][2].append(y[iy]-x[ix])
                    ix += 1
                    if ix+1 >= x.shape[0]: break 
                    if y[iy+1] > x[ix+1]:
                        isi[-1][2].append(x[ix]-y[iy])
                        iy += 1
                        if iy+1 == y.shape[0]: break
                    else:
                        x2y = False
                else:
                    while not(y[iy] <= x[ix] and y[iy+1] > x[ix]):
                        iy += 1
                        if iy+1 == y.shape[0]: break
                    isi[-1][2].append(x[ix]-y[iy])
                    iy += 1
                    if iy+1 >= y.shape[0]: break
                    if x[ix+1] > y[iy+1]:
                        isi[-1][2].append(y[iy]-x[ix])
                        ix += 1
                        if ix+1 == x.shape[0]: break
                    else:
                        x2y = True
            isi[-1][2] = array(isi[-1][2])
    return isi

# def _full_ISI_fit_hist(spikes, dist, isimin:float=0, isimax:float=self.isimax,):
