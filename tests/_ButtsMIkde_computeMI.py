from numpy import *
from matplotlib.pyplot import *
import logging
import sys, os
# pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
sys.path.append( pth )

from ButtsMI.butts_mi_kde   import ButtsMIkde

logging.basicConfig(
        format='%(asctime)s:%(lineno)-6d%(levelname)-8s:%(message)s',\
        level=logging.DEBUG)

# bmikde = ButtsMIkde(reduceBOS=False,jitNbin=49)
bmikde = ButtsMIkde(reduceBOS=False,jitNbin=49,isimax=10.)

ft = arange(20)
spk = arange(0,3600,20.)
spikes = [ spk+f*5e-2+random.rand(spk.shape[0])*50e-2  for f in ft]
spikes = [ sort(concatenate((
        spk+f/5,#+random.rand(spk.shape[0])/7,
        random.rand(spk.shape[0]//5)*3600
        ))) for f in ft       ]

dFdT = bmikde._collect_dFdT(ft, spikes)
# dFmin, dFmax = amin(dFdT[:,0])*0.5,amax(dFdT[:,0])*1.5
# dTmin, dTmax = amin(dFdT[:,1])*0.5,amax(dFdT[:,1])*1.5

# dFmin, dFmax = 1. , 20.
# dTmin, dTmax = 0. , 10.

dFmin, dFmax = None , None
dTmin, dTmax = None , None

ret = bmikde._computeMI(dFdT,dFmin=dFmin, dFmax=dFmax, dTmin=dTmin, dTmax=dTmax)
if ret is None:
   sys.stderr.write("Test failed\n")
   exit(1)
res,err = ret
print(f"MI:{res},\tERROR:{err}")
if res < 0.:
   sys.stderr.write("MI negative")
   exit(1)
        
