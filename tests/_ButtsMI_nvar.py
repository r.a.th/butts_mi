from numpy import *
import logging
import sys, os
# pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
sys.path.append( pth )


from ButtsMI import ButtsMI
logging.basicConfig(
        format='%(asctime)s:%(lineno)-6d%(levelname)-8s:%(message)s',\
        level=logging.DEBUG)

bmi = ButtsMI()

x = arange(0,101,1)
logging.debug("_nvar() first test")
h,b,nbins = ButtsMI()._nvar(x, 0, 100, 3, 5, 'test',100)
if nbins != 33:
    sys.stderr.write(f'First test _nvar() fails. Function does not return 33 bins')
    exit(1)
logging.debug("_nvar() second test")
h,b,nbins = ButtsMI()._nvar(x, 0, 100, 3, 5, 'test', 5)
if nbins != 17:
    sys.stderr.write(f'Second test _nvar() fails. Function does not return 17 bins')
    exit(1)

