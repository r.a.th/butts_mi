from numpy import *
from matplotlib.pyplot import *
import logging
import sys, os
# pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
pth = os.path.split( os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] )[0] 
sys.path.append( pth )

from scipy.stats  import gaussian_kde as scikde
from scipy.integrate import dblquad      as sci2int

# pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
sys.path.append( pth )

from ButtsMI.butts_mi_kde   import ButtsMIkde

logging.basicConfig(
        format='%(asctime)s:%(lineno)-6d%(levelname)-8s:%(message)s',\
        level=logging.INFO)

# bmikde = ButtsMIkde(reduceBOS=False,jitNbin=49)
bmikde = ButtsMIkde(reduceBOS=False,jitNbin=49,isimax=10.)

ft = arange(20)
spk = arange(0,3600,20.)
spikes = [ sort(concatenate((
        spk+f/5,#+random.rand(spk.shape[0])/7,
        random.rand(spk.shape[0]//5)*3600
        ))) for f in ft       ]


figure(1,figsize=(10,10))
for nid,s in enumerate(spikes):
    plot(s,nid*ones(s.shape[0]),'.')
xlim(0, 180)

dFdT = bmikde._collect_dFdT(ft, spikes)
# dFmin, dFmax = amin(dFdT[:,0])*0.5,amax(dFdT[:,0])*1.5
# dTmin, dTmax = amin(dFdT[:,1])*0.5,amax(dFdT[:,1])*1.5

print(amin(dFdT[:,0]),amax(dFdT[:,0]) )
print(amin(dFdT[:,1]),amax(dFdT[:,1]) )

dFmin, dFmax = 1. , 20.
dTmin, dTmax = 0. , 10.

_kde    = scikde(dFdT.T)
_kde_dF = scikde(dFdT[:,0].flatten())
_kde_dT = scikde(dFdT[:,1].flatten())

gridplot = 25
figure(2,figsize=(24,10))

y,_error_ = sci2int(\
            bmikde._zero_nan_kde,
            dFmin, dFmax,
            dTmin, dTmax,
            args=(_kde, _kde_dF,_kde_dT),
            # epsrel = 1e-60,
            # epsabs = 1e-60
        )
suptitle(f"MI={y}, error:{_error_}")

ax_0 = subplot2grid( (3,5), (0,0),rowspan=2,colspan=2)
h,x,y = histogram2d(
    dFdT[:,0].flatten(),
    dFdT[:,1].flatten(),
    bins  = gridplot,
    range = array([
        [dFmin, dFmax],
        [dTmin, dTmax]
    ])
)
h = h/sum(h)/(x[1]-x[0])/(y[1]-y[0])
h = ax_0.pcolormesh(x,y,h.T,cmap = get_cmap('rainbow'),vmin=0.)
hax = subplot2grid( (3,5), (2,0))
colorbar(h,cax = hax)

ax_pdfdt= subplot2grid( (3,5), (0,3),rowspan=2,colspan=2,sharex=ax_0,sharey=ax_0)
ax_pdf  = subplot2grid( (3,5), (2,3),colspan=2,sharex=ax_0)
ax_pdt  = subplot2grid( (3,5), (0,2),rowspan=2,sharey=ax_0)
dF_flat = linspace(dFmin, dFmax,gridplot)
dT_flat = linspace(dTmin, dTmax,gridplot)

x,y = meshgrid(
        (dF_flat[:-1]+dF_flat[1:])/2,
        (dT_flat[:-1]+dT_flat[1:])/2,
      )


xy  = column_stack((x.flatten(),y.flatten()))

pdFdT = []
pdF   = []
pdT   = []

for dF, dT in xy:
    xpdFdT, = _kde([dF,dT])
    pdFdT.append(xpdFdT)

for dF in dF_flat:
    xpdF,   = _kde_dF(dF)
    pdF.append([dF,xpdF])

for dT in dT_flat:
    xpdT,   = _kde_dT(dT)
    pdT.append([dT,xpdT])

pdFdT = array(pdFdT).reshape(x.shape)
h = ax_pdfdt.pcolormesh(x,y,pdFdT,cmap = get_cmap('rainbow'),vmin=0.)
hax = subplot2grid( (3,5), (2,1))
colorbar(h,cax = hax)

pdF, pdT = array(pdF),array(pdT)

ax_pdf.plot(pdF[:,0],pdF[:,1],'-o')
h,b = histogram(dFdT[:,0],bins=dF_flat)
b0  = b[1]-b[0]
b   = (b[:-1]+b[1:])/2
ax_pdf.plot(b,h/sum(h)/b0,'k-')

ax_pdt.plot(pdT[:,1],pdT[:,0],'-o')
h,b = histogram(dFdT[:,1],bins=dT_flat)
b0  = b[1]-b[0]
b   = (b[:-1]+b[1:])/2
ax_pdt.plot(h/sum(h)/b0,b,'k-')
show()
