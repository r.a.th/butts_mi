from numpy import *
from matplotlib.pyplot import *
import logging
import sys, os
# pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
sys.path.append( pth )


from ButtsMI import ButtsMI
logging.basicConfig(
        format='%(asctime)s:%(lineno)-6d%(levelname)-8s:%(message)s',\
        level=logging.INFO)
        
bmi = ButtsMI(reduceBOS=False,jitNbin=49)
ft = arange(40)
spk = arange(0,3600,20.)

#>> Exact sequence
figure(1)
imax = subplot2grid((2,2),(0,1),rowspan=2)

spikes = [ spk+f*5e-2  for f in ft]
spax = subplot2grid((2,2),(0,0))
for si,s in enumerate(spikes):
        plot(s,ones(s.shape[0])*si,'k.')
ret = bmi(ft,spikes)
if ret is None: print(ret)
else:
        MI,jitrange,meanjMI,stdjMI = ret
        print(MI)
        imax.errorbar(jitrange, meanjMI, fmt='.', yerr=stdjMI,label=f'ISI= 50ms, MI={MI:0.4f}')

spikes = [ spk+f*1e-1 for f in ft]
subplot2grid((2,2),(1,0),sharex=spax)
for si,s in enumerate(spikes):
        plot(s,ones(s.shape[0])*si,'k.')
ret = bmi(ft,spikes)
if ret is None: print(ret)
else:
        MI,jitrange,meanjMI,stdjMI = ret
        print(MI)
        imax.errorbar(jitrange, meanjMI, fmt='.', yerr=stdjMI,label=f'ISI=100ms, MI={MI:0.4f}')

imax.set_xscale("log")
imax.legend(loc='best')


figure(2)
imax = subplot2grid((2,2),(0,1),rowspan=2)

spikes = [ spk+f*1e-1+random.random(spk.shape[0])*5 for f in ft]
spax = subplot2grid((2,2),(0,0))
for si,s in enumerate(spikes):
        plot(s,ones(s.shape[0])*si,'k.')
ret = bmi(ft,spikes)
if ret is None: print(ret)
else:
        MI,jitrange,meanjMI,stdjMI = ret
        print(MI)
        imax.errorbar(jitrange, meanjMI, fmt='.', yerr=stdjMI,label=f'ISI=100ms, MI={MI:0.4f} + noise')


#>> Same analysis but with adaptive histogram size
bmi.tp2maxisi = 2
bmi.t_inbin_min = 2
spikes = [ spk+f*1e-1+random.random(spk.shape[0])*5 for f in ft]
subplot2grid((2,2),(1,0),sharex=spax)
for si,s in enumerate(spikes):
        plot(s,ones(s.shape[0])*si,'k.')
ret = bmi(ft,spikes)
if ret is None: print(ret)
else:
        MI,jitrange,meanjMI,stdjMI = ret
        print(MI)
        imax.errorbar(jitrange, meanjMI, fmt='.', yerr=stdjMI,label=f'ISI=100ms, MI={MI:0.4f} + noise, adap')


imax.set_xscale("log")
imax.legend(loc='best')
show()
