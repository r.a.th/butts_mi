from numpy import *
import logging
import sys, os
# pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
sys.path.append( pth )



logging.basicConfig(
        format='%(asctime)s:%(lineno)-6d%(levelname)-8s:%(message)s',\
        level=logging.DEBUG)
from ButtsMI import ButtsMI
bmi = ButtsMI(reduceBOS=False)
p_dT_dF = [
    array([0.,0.,1.,0.]),
    array([1.,0.,0.,0.]),
    array([0.,1.,0.,0.]),
    array([0.,0.,0.,1.]),
]
p_dF = array([0.1,0.2,0.4,0.3]) 
p_dT = array([0.2,0.4,0.1,0.3])
mi = bmi._computeMIfromPdTdF(p_dF, p_dT, p_dT_dF)
print(mi)

p_dT_dF = [
    array([0.,1.]),
    array([1.,0.]),
]
p_dF = array([0.5,0.5]) 
p_dT = array([0.5,0.5])
mi = bmi._computeMIfromPdTdF(p_dF, p_dT, p_dT_dF)
print(mi)

p_dT_dF = [
    array([0.,0.,1.,0.]),
    array([1.,0.,0.,0.]),
    array([0.,1.,0.,0.]),
    array([0.,0.,0.,1.]),
]
p_dF = array([0.25,0.25,0.25,0.25]) 
p_dT = array([0.25,0.25,0.25,0.25])
mi = bmi._computeMIfromPdTdF(p_dF, p_dT, p_dT_dF)
print(mi)
