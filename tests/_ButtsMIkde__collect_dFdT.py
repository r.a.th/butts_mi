from numpy import *
from matplotlib.pyplot import *
import logging
import sys, os
# pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
sys.path.append( pth )

from ButtsMI.butts_mi_kde import ButtsMIkde
bmikde = ButtsMIkde(reduceBOS=False,jitNbin=49)

ft = arange(40)
spk = arange(0,3600,20.)
spikes = [ spk+f*5e-2  for f in ft]

ret = bmikde._collect_dFdT(ft,spikes)
print(ret)
print(ret.shape)

