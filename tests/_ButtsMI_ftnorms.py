from numpy import *
import logging
import sys, os
# pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
pth = os.path.split( os.path.split( os.path.abspath(__file__) )[0] )[0] 
sys.path.append( pth )


from ButtsMI import ButtsMI
logging.basicConfig(
        format='%(asctime)s:%(lineno)-6d%(levelname)-8s:%(message)s',\
        level=logging.DEBUG)
        
bmi = ButtsMI()

x = arange(5)
logging.debug("_ftnorms() first test")
d = bmi._ftnorms(x).astype(int)
if d.tolist() != [1,2,3,4,1,2,3,1,2,1]:
    sys.stderr.write(f'First test _ftnorms fails.')
    exit(1)

logging.debug("_ftnorms() second test")
d = bmi._ftnorms(x,order=1).astype(int)
if d.tolist() != [1,2,3,4,1,2,3,1,2,1]:
    sys.stderr.write(f'Second test _ftnorms fails.')
    exit(1)

x = x[::-1]
logging.debug("_ftnorms() third test")
d = bmi._ftnorms(x,order=1).astype(int)
if d.tolist() != [1,2,3,4,1,2,3,1,2,1]:
    sys.stderr.write(f'third test _ftnorms fails.')
    exit(1)

logging.debug("_ftnorms() 4rd test")
d = bmi._ftnorms(x,order=3).astype(int)
if d.tolist() != [1,2,3,4,1,2,3,1,2,1]:
    sys.stderr.write(f'4rd test _ftnorms fails.')
    exit(1)
