# Butts's Mutual Information analysis for a set of spike trains

This module computes Mutual Information (MI) between interspike/interburst intervals (ISI) of _different_ neurons in a set of neurons and differences in neuron features.
In Butts's [original paper](https://doi.org/10.1523/JNEUROSCI.21-03-00961.2001), a feature is a position of retinal ganglion cell of developing retina during the period of retinal spontaneous activity (retinal waves).
Because the correlation between activity decays with distance, ISIs convey a finite amount of information about the distance between spike sources (neurons) in the retina.
This information is extracted by subcortical regions, such as the visual thalamus and superior calculus, to refine connections from the retina and create exact retinotopic maps in both nuclei.
![](MI_dFDT.svg)

Butts also suggested an elegant method to assess the timescale of informative correlation by jittering recorded spikes and computing MI. The magnitude of the jitter, when MI starts to decay, defines the lower bound of the timescale of informative correlations.

This analysis can be extended to any feature, not necessarily differences in positions, and therefore, can be useful for assessing information conveyed in spike differences from a set of neurons.


---

This module provides two classes `ButtsMI` and '`ButtsMIkde`

The first one, `ButtsMI`, computes MI and jitter-based correlational timescale using histogram method. The class computes MI based on conditional distributions (Eq. 6 in the paper):
$$
I(\Delta f,\Delta t) = \sum_{\Delta f} p(\Delta f) \sum_{\Delta t} p(\Delta t| \Delta f) ~ \log_2 \left[ \frac{p(\Delta t| \Delta f)}{p(\Delta t)}\right]
$$
where, $\Delta f$ - difference between features, $\Delta t$ -interspike/interburst intervals. **Nonte.** To applay correction (Eq. 2 in the paper), set `CorrectMI=True` and to stop computation if MI is negative set `BlockCorrect=True`.


The second one is `ButtsMIkde` and it does the same computations based on kernel density estimators. The class computes MI as 
$$
I[\Delta f, \Delta T]= \iint_{V=(\Delta f,\Delta t)} P_{(\Delta f,\Delta t)}(x,y) \log{\frac{P_{(\Delta f,\Delta t)}(x,y)}{P_{\Delta f}(x)P_{\Delta t}(y)}} dx dy
$$
where $P_{(\Delta f,\Delta t)}(\Delta f,\Delta t)$ is an 2D estimator of probability dencity for join distribution of $(\Delta f,\Delta t)$, and $P_{\Delta f}(\Delta f)$ and $P_{\Delta t}(\Delta t)$ are marginal 1D propability density estimators.






## An example: population of modulated Poisson processes.


```python
%matplotlib inline
from numpy import *
from matplotlib.pyplot import *
getnextspike = lambda t,r: t+random.exponential(scale=1./r)
Nneurons = 50
baserate =  0.1  # spikes per second
peakrate = 10.   # spikes per second
thr_rate =  3.   # minimal rate for burst onset
modper   = 20.   # 20 second period 
duration = 40*60 # in seconds
nrnphase = [ nid*pi/2./Nneurons for nid in range(Nneurons)]
tscan    = arange(0,modper,1)+1
spikes   = []
for nid,dphy in enumerate(nrnphase):
    t = 0
    spikes.append([])
    while True:
        r = peakrate*cos(2*pi*t/modper+dphy)
        if r < thr_rate: r=baserate
        t1 = getnextspike(t,r)
        r0 = peakrate*cos(2*pi*(t+tscan)/modper+dphy)
        t0 = tscan[where(r0 >= thr_rate)]
        
        t = min([t1,t+t0[0]])
        if t >= duration: break
        spikes[-1].append(t)
    spikes[-1] = array(spikes[-1])
figure(figsize=(24,10))
for nid,s in enumerate(spikes):
    plot(s,nid*ones(s.shape[0]),'.')
xlim(1000, 1060)
show()
```


    
![png](output_2_0.png)
    


### Computing MI and Informative timescale of _Bursts_

By default `ButtsMI` class extracts burst onset time (BOST) first, and then computes MI.

**Note** If any histogram containes zero in any bin, the code will generate `RuntimeWarning`, which should be ignored. `nan` are taken into account by `nansum` function.



```python
from ButtsMI import ButtsMI
bmi = ButtsMI(jTparallel=True)
ret = bmi(nrnphase,spikes)
if ret is None:
    MI = None
else:
    MI,jitrange,meanjMI,stdjMI = ret
    
    

if not MI is None:
    figure(figsize=(16,8))
    title(f'MI={MI}',fontsize=21)
    if stdjMI is None:
        plot(jitrange,meanjMI,'ko')
    else:
        errorbar(jitrange,meanjMI,yerr=stdjMI,fmt='k-')
    
    xscale('log')
    show()
```

    
![png](output_4_1.png)
    


### Computing MI and Informative timescale of _Spikes_

If the argument `reduceBOS` of the class or class atribute is set to False, the `ButtsMI` will compute MI without BOST filtering.


```python
bmi.reduceBOS = False
ret = bmi(nrnphase,spikes)
if ret is None:
    MI = None
else:
    MI,jitrange,meanjMI,stdjMI = ret
    
    

if not MI is None:
    figure(figsize=(16,8))
    title(f'MI={MI}',fontsize=21)
    if stdjMI is None:
        plot(jitrange,meanjMI,'o')
    else:
        errorbar(jitrange,meanjMI,yerr=stdjMI,fmt='k-')
    
    xscale('log')
    show()
```

    
![png](output_6_1.png)
    


## Computing MI and informative timescale of _Bursts_ using __Kernel Density Estimators__

This examples shows how to use `ButtsMIkde` class to compute mutual information.


```python
import logging
logging.basicConfig(
    format='%(asctime)s:%(lineno)-6d%(levelname)-8s:%(message)s',\
    level=logging.INFO)

from ButtsMI import ButtsMIkde
bmikde = ButtsMIkde(jitrep=5,jitNbin=15)
ret    = bmikde(nrnphase,spikes)
if ret is None:
    MI = None
else:
    MI,jitrange,meanjMI,stdjMI = ret

if not MI is None:
    figure(figsize=(16,8))
    title(f'MI={MI}',fontsize=21)
    if stdjMI is None:
        plot(jitrange,meanjMI,'ko')
    else:
        errorbar(jitrange,meanjMI,yerr=stdjMI,fmt='k-')
    
    xscale('log')
    show()
```

    
![png](output_8_1.png)
    

